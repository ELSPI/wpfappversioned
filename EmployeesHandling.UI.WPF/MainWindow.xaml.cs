﻿using EmployeesHandling.DatabaseLayer;
using System;
using System.Windows;
using System.Windows.Controls;

namespace EmployeesHandling.UI.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            vmdl = null;
        }
      
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vmdl = this.DataContext as WorkingViewModel;
            if (vmdl == null)
                return;

            vmdl.OnLoaded();
        }

        private void ListView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender == null)
                return;
            if (sender as ListView == null)
                return;

            if ((sender as ListView).SelectedItem == null)
                return;

            Console.WriteLine(((sender as ListView).SelectedItem as Employee).EmpId);
            if (vmdl == null)
                return;

            vmdl.CurrentEmployee = (sender as ListView).SelectedItem as Employee;
        }


       
        private void MyMainWindow_Drop(object sender, DragEventArgs e)
        {
            MessageBox.Show("Dragging...");
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                var FilePaths = e.Data.GetData(DataFormats.FileDrop) as string[];
                var shortNameSplit = FilePaths[0].Split('\\');
                string shortName = shortNameSplit[shortNameSplit.Length - 1];
                MessageBox.Show(shortName);
                CurrentEmployeeLastName.Text = shortName;
            }

        } 
        
        WorkingViewModel vmdl;

        private void ButtonDrop_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MessageBox.Show("mouse  up");
        }

        private void ButtonDrop_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            MessageBox.Show("mouse enter");
        }
    }
}

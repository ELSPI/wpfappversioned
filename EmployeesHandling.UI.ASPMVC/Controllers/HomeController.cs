﻿using EmployeesHandling.DatabaseLayer;
using EmployeesHandling.UI.ASPMVC.Models;
using System.Web.Mvc;

namespace EmployeesHandling.UI.ASPMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string button)
        {
            MyModel _myModel = new MyModel();
            switch (button)
            {
                case "read":
                    _myModel.Read();
                    break;
                default:
                    break;
            }
            return View(_myModel);
        }

        public ActionResult About(string button)
        {
            ViewBag.Message = "Your application description page.";
            MyModel _myModel = new MyModel();
            
            
            switch (button)
            {
                case "10":
                    _myModel.CalcRandom(10);
                    break;
                case "100":
                    _myModel.CalcRandom(100);
                    break;
                case "1000":
                    _myModel.CalcRandom(1000);
                    break;
                default:
                    break;
            }

            return View(_myModel);
        }

        public ActionResult Contact(string button)
        {
            MyModel _myModel = new MyModel();
            switch (button)
            {
                case "read":
                    _myModel.Read();
                    break;
                default:
                    break;
            }
            return View(_myModel.ListEmployees);
        }


        [HttpPost]
        public JsonResult InsertEmployee(Employee employee)
        {
            MyModel _myModel = new MyModel();
            _myModel.Insert(employee);
            
            return Json(employee);
        }

        [HttpPost]
        public JsonResult UpdateEmployee(Employee employee)
        {
            MyModel _myModel = new MyModel();
            _myModel.Update(employee);

            return Json(employee);
        }

    }
}
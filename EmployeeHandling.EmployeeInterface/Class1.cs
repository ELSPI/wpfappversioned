﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeesHandling.EmployeeInterface
{
    public partial interface IEmployeeEntities
    {
        void Insert(object obj);
        void Update(object obj);
        IEnumerable<object> Read();
        int EmpId { get; set; }
        string HREmpId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Address { get; set; }
        string City { get; set; }
    }

}

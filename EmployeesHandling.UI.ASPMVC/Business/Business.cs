﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeesHandling.UI.ASPMVC.Business
{
    public class BusinessLocalInMVC
    {
        public BusinessLocalInMVC()
        {
            rnd = new Random();
        }
        public int CalcRandom(int range)
        {

            return rnd.Next(range/10, range);

        }
        private Random rnd;
    }
}
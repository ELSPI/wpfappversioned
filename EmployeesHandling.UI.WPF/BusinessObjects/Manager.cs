﻿using EmployeesHandling.BusinessLogic;

namespace EmployeesHandling.Components
{
    /// <summary>  
    ///  This class implements all of the singleton objects of the main classes
    ///  to be implemented in the code. So these objects are globally available through
    ///  the Instance pointer. This way you are sure you don't have duplicates.
    /// </summary> 
    public class Manager
    {
        #region Singleton

        private static Manager m_Instance;

        public static Manager Instance
        {
            get
            {
                if (m_Instance == null)
                    m_Instance = new Manager();
                return m_Instance;
            }
        }

        #endregion

        #region Members private

        private MyBusinessLogic myBusinessLogic = new MyBusinessLogic();

        #endregion

        #region Properties

        internal MyBusinessLogic MyBusiness
        {
            get { return myBusinessLogic; }
            set { myBusinessLogic = value; }
        }

        #endregion
        
    }
}

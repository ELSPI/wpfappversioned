﻿using System;
using System.ComponentModel;
using System.Windows.Input;

namespace EmployeesHandling.Components
{
    /// <summary>
    /// Define a Custom WPF Command that implements ICommand.
    /// This is used to transmit (mainly) Button commands from the WPF UI to the ViewModel.
    ///  DO NOT CHANGE ANYTHING IN THIS CODE.
    /// </summary>
    public class MyCommand : ICommand, IDisposable
    {
        #region Definitions

        #endregion Definitions

        #region Variables

        private Action<object> _execute;
        private Predicate<object> _canExecute;
        private INotifyPropertyChanged _propertyChangeableObject;
        private EventHandler _canExecuteChangedEventHandler;

        #endregion Variables

        #region Construction/Destruction/Initialization

        /// <summary>
        /// Initialize a new Instance of MyCommand object.
        /// </summary>
        public MyCommand()
            : this(null, null)
        {
        }

        /// <summary>
        /// Initialize a new Instance of MyCommand object.
        /// </summary>
        /// <param name="execute">Delegate that encapsulate a method with one parameter and no return value.</param>
        public MyCommand(Action<object> execute)
            : this(null, execute)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MyCommand">MyCommand</see> class.
        /// This instance will listen for property change events on the given object. Once
        /// an event is caught the ICommand.CanExecuteChanged event is raised to inform the
        /// source it should recall the CanExecute method.
        /// </summary>
        /// <param name="canExecute">The can execute.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="propertyChangeableObject">The property changeable object.</param>
        public MyCommand(Predicate<object> canExecute, Action<object> execute, INotifyPropertyChanged propertyChangeableObject)
            : this(canExecute, execute)
        {
            _propertyChangeableObject = propertyChangeableObject;

            if (_propertyChangeableObject != null)
            {
                // Watch for property change events to raise the CanExecuteChanged event
                _propertyChangeableObject.PropertyChanged += this.PropertyChangeableObject_PropertyChanged;
            }
        }

        /// <summary>
        /// Initialize a new Instance of MyCommand object.
        /// </summary>
        /// <param name="execute">Delegate that encapsulate a method with one parameter and no return value.</param>
        /// <param name="canExecute">Represents the method that defines a set of criteria and determines whether the specified object meets those criteria.</param>
        public MyCommand(Predicate<object> canExecute, Action<object> execute)
        {
            this._execute = execute;
            this._canExecute = canExecute;
        }

        #endregion Construction/Destruction/Initialization

        #region ICommand Members

        /// <summary>
        /// Determines whether this MyCommand can execute in its current state.
        /// </summary>
        /// <param name="parameter">A user defined data type.</param>
        /// <returns>true if the command can execute on the current command target; otherwise, false.</returns>
        public virtual bool CanExecute(object parameter)
        {
            return this._canExecute == null || this._canExecute(parameter);
        }

        /// <summary>
        /// Occurs when changes to the command source are detected by the command manager.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                _canExecuteChangedEventHandler = (EventHandler)Delegate.Combine(this._canExecuteChangedEventHandler, value);
                CommandManager.RequerySuggested += value;
            }

            remove
            {
                _canExecuteChangedEventHandler = (EventHandler)Delegate.Remove(this._canExecuteChangedEventHandler, value);
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// Executes the MyCommand on the current command target.
        /// </summary>
        /// <param name="parameter">User defined parameter to be passed to the handler.</param>
        public void Execute(object parameter)
        {
            if (this._execute != null)
            {
                this._execute(parameter);
                CommandSucceeded = true;
            }
        }

        #endregion ICommand Members

        #region Properties

        /// <summary>
        /// Defines whether the command has completed its processing with success or not.
        /// The initial value is false.
        /// </summary>
        public bool CommandSucceeded { get; set; }

        /// <summary>
        /// Delegate handling the command's action itself. Will be run only if
        /// CanExecute returns true.
        /// </summary>
        public Action<object> ExecuteDelegate
        {
            get { return this._execute; }
            set { this._execute = value; }
        }

        /// <summary>
        /// Delegate handling the CanExecute method of the command.
        /// From the result of this method call (boolean result) the UI
        /// will decide whether to enable or not the component(s) that is(are)
        /// bound to it.
        /// </summary>
        public Predicate<object> CanExecuteDelegate
        {
            get { return this._canExecute; }
            set { this._canExecute = value; }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Sets the CanExecuteDelegate and ExecuteDelegate to null to prevent any un-wanted calls.
        /// </summary>
        public void Dispose()
        {
            _canExecute = null;
            _execute = null;
            if (_propertyChangeableObject != null)
            {
                _propertyChangeableObject.PropertyChanged -= this.PropertyChangeableObject_PropertyChanged;
                _propertyChangeableObject = null;
            }
        }

        /// <summary>
        /// Raises the can execute changed event.
        /// </summary>
        public virtual void RaiseCanExecuteChanged()
        {
            this.OnCanExecuteChanged();
        }

        /// <summary>
        /// Called when property changeable object's property changed.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="args">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void PropertyChangeableObject_PropertyChanged(object source, PropertyChangedEventArgs args)
        {
            this.OnCanExecuteChanged();
        }

        /// <summary>
        /// Raises the CanExecuteChanged event
        /// </summary>
        private void OnCanExecuteChanged()
        {
            if (_canExecuteChangedEventHandler != null)
            {
                _canExecuteChangedEventHandler(this, EventArgs.Empty);
            }
        }

        #endregion Methods
    }
}

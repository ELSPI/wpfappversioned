﻿using EmployeesHandling.Components;
using EmployeesHandling.BusinessLogic;
using EmployeesHandling.DatabaseLayer;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace EmployeesHandling.UI.WPF
{
    /// <summary>
    /// MVVM implementation.<br/>
    /// This is the base plate where your views lie and connect by reflexion.<br/>
    /// It makes ties (bindings) to the core functions and classes of the code.<br/>
    /// The goal is to have almost no intelligence in this class, but only to be a hub for the UI signals.
    /// </summary>
    public class WorkingViewModel : INotifyPropertyChanged
    {

        #region Construction/Destruction/Initialization
        /// <summary>
        /// Initialize a new Instance of WorkingViewModel object.
        /// </summary>
        public WorkingViewModel()
        {
            _CurrentEmployee = new Employee();
            ButtonsCommand = new MyCommand { ExecuteDelegate = OnButtons };
            string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["LearnEFEntities"].ConnectionString;
            _ListEmployees = new ObservableCollection<Employee>();
            Console.WriteLine(connectionStr);
            BindSingletons();
        }

        public void BindSingletons()
        {
            //counterDW = Manager.Instance.MyBusiness.counterDW;
            //resultDW = Manager.Instance.MyBusiness.resultDW;
            //CurrentEmployee = Manager.Instance.MyBusiness.CurrentEmployee;
        }
        public void OnLoaded()
        {
            //OnButtons("read");
        }
        ~WorkingViewModel()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Events

        #region public


        #endregion

        #region private

        private void RaisePropertyChanged(string v)
        {
            OnPropertyChanged(v);
        }
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void OnButtons(object obj)
        {

            var parameter = obj as string;

            switch (parameter)
            {
                case "searchZefix":
                    resultDW.Value = Manager.Instance.MyBusiness.ExecuteSearchZefix("ELCA");
                    break;
                case "clearscreen":
                    ListEmployees.Clear();
                    CurrentEmployee = null;
                    break;
                case "read":
                    ListEmployees = Manager.Instance.MyBusiness.Read().ToObservableCollection();
                    if (ListEmployees.Count() == 0)
                        return;
                    CurrentEmployee = ListEmployees.First();
                    break;
                case "insert":
                    Manager.Instance.MyBusiness.Insert(CurrentEmployee);
                    break;
                case "update":
                    Manager.Instance.MyBusiness.Update(CurrentEmployee);
                    break;
                default:
                    break;
            }
        }


        #endregion

        #endregion

        #region Members private
        private Employee _CurrentEmployee;
        private ObservableCollection<Employee> _ListEmployees;
        #endregion

        #region Properties

        public MyCommand ButtonsCommand { get; private set; }
        public DataWrapper<int> counterDW { get; set; }
        public DataWrapper<string> resultDW { get; set; }
        public Employee CurrentEmployee { 
            get { return _CurrentEmployee; }
            set
            {
                _CurrentEmployee = value;
                RaisePropertyChanged("CurrentEmployee");
            }
        }
        public ObservableCollection<Employee> ListEmployees
        {
            get { return _ListEmployees; }
            set
            {
                _ListEmployees = value;
                RaisePropertyChanged("ListEmployees");
            }
        }
        #endregion


    }
}

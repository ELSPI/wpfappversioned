﻿using EmployeesHandling.DatabaseLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace EmployeesHandling.BusinessLogic
{
    public class MyBusinessLogic
    {
        public MyBusinessLogic()
        {
            m_myDbAccess = new MyDatabaseLayer();
        }

        private MyDatabaseLayer m_myDbAccess;
        const string webSite = "https://www.zefix.ch/";
        const string path = "https://www.zefix.ch/fr/search/entity/list";
        const string pathComplete = "https://www.zefix.ch/global/search/entity/list?name={search_term_string}&searchType=exact";
        const string replaceStr = "{search_term_string}";

        public string ExecuteSearchZefix(string str)
        {
            StringBuilder retSB = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            sb.Append("https://www.zefix.ch/fr/search/entity/list?name=");
            sb.Append(str);
            sb.Append(@"&searchType=exact");
            sb.Clear();
            sb.Append(pathComplete.Replace(replaceStr, str));


            using (var webClient = new System.Net.WebClient())
            {
                var json = webClient.DownloadString(sb.ToString());
                // Now parse with JSON.Net
            }

            retSB.Append("Requesting : " + sb.ToString());
            retSB.Append("\n");


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sb.ToString());

            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.0)";

            WebResponse webResp = request.GetResponse();

            retSB.Append("Status : " + ((HttpWebResponse)webResp).StatusDescription);
            retSB.Append("\n");


            // Get the stream containing content returned by the server. 
            // The using block ensures the stream is automatically closed. 
            using (Stream dataStream = webResp.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string responseFromServer = reader.ReadToEnd();
                // Display the content.  

                retSB.Append(responseFromServer);

            }

            // Close the response.  
            webResp.Close();

            Console.WriteLine(retSB);
            return retSB.ToString();
        }

        public string ReadMockup()
        {
            return "toto";
        }
        public IEnumerable<Employee> Read()
        {

            return m_myDbAccess.ReadFromDB();
            

            /*Hardcoded for example*/
            List<Employee> list = new List<Employee>();
            Employee emp = new Employee();
            emp.EmpId = 1;
            emp.LastName = "Smith";
            emp.FirstName = "John";
            emp.City = "Bern";
            emp.Address = "Seilerstrasse 4";

            list.Add(emp);
            Employee emp2 = new Employee();
            emp2.EmpId = 2;
            emp2.LastName = "Jurgens";
            emp2.FirstName = "Udo";
            emp2.City = "Zurich";
            emp2.Address = "Flurstrasse";

            list.Add(emp2);
            return list; 
            return null;// dbAccess.ReadFromDB();

        }
        public void Insert(Employee emp)
        {
            m_myDbAccess.AddToDB(emp);
        }
        public void Update(Employee emp)
        {
            m_myDbAccess.UpdateDB(emp);
        }

    }


   
}

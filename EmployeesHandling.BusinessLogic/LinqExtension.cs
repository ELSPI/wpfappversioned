﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeesHandling.BusinessLogic
{
    public static class CustomExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> target, Action<T> todo)
        {
            foreach(var item in target)
            {
                todo(item);
            }
        }
    
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> col)
        {
            return new ObservableCollection<T>(col);
        }

    }

}

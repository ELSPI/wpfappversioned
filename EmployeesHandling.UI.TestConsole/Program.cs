﻿using EmployeesHandling.Test.Nunit.FLAUI;
using EmployeesHandling.Test.Nunit.TestStackWhite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace EmployeesHandling.UI.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestFLAUI();
            TestStackWhite();


        }
        static void TestFLAUI()
        {
            NunitFLAUI _nunitFLA = new NunitFLAUI();
            _nunitFLA.OneTimeInit();
            _nunitFLA.TestSimulateDragDropUI();
            //_nunitFLA.TestClick();

        }

        static void TestStackWhite()
        {
            NunitTestStackWhite _nunitStackWhite = new NunitTestStackWhite();
            _nunitStackWhite.OneTimeInit();
            //_nunitStackWhite.TestClick();
            _nunitStackWhite.TestSimulateDragDropUI();
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace EmployeesHandling.DatabaseLayer
{

    public class MyDatabaseLayer
    {
        public MyDatabaseLayer()
        {


        }

        public List<Employee> ReadFromDB()
        {
            using (var db = new LearnEFEntities())
            {
                return db.Employees.ToList();
            }
        }

        public void UpdateDB(Employee emp)
        {
            if (emp == null)
                return;
            if (emp.EmpId <= 0)
                return;

            using (var db = new LearnEFEntities())
            {
                var empToUpdate = db.Employees.First(e => e.EmpId == emp.EmpId);
                if (emp.Address != null && emp.Address != string.Empty)
                    empToUpdate.Address = emp.Address;
                if (emp.LastName != null && emp.LastName != string.Empty)
                    empToUpdate.LastName = emp.LastName;
                if (emp.FirstName != null && emp.FirstName != string.Empty)
                    empToUpdate.FirstName = emp.FirstName;
                if (emp.City != null && emp.City != string.Empty)
                    empToUpdate.City = emp.City;

                db.SaveChanges();
            }
        }

        public void AddToDB(Employee emp)
        {
            if (emp == null)
                return;

            using (var db = new LearnEFEntities())
            {
                var empToAdd = new Employee();
                empToAdd.EmpId = 0;
                if (emp.Address != string.Empty)
                    empToAdd.Address = emp.Address;
                if (emp.LastName != string.Empty)
                    empToAdd.LastName = emp.LastName;
                if (emp.FirstName != string.Empty)
                    empToAdd.FirstName = emp.FirstName;
                if (emp.City != string.Empty)
                    empToAdd.City = emp.City;

                db.Employees.Add(empToAdd);
                db.SaveChanges();
            }

        }

    }
}

﻿
using System;
using System.ComponentModel;

namespace EmployeesHandling.Components
{
    public class DataWrapper<T> : INotifyPropertyChanged
    {
        
        #region Constructor

        public DataWrapper()
        {

        }
        public DataWrapper(T _default)
        {
            m_Value = _default;
        }

        #endregion

        #region Members
        
        private T m_Value;
        public T Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                m_Value = value;
                OnPropertyChanged("Value");
                OnValueChanged(m_Value);

            }
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;
        public delegate void ValueChangedEventHandler(T obj);
        public event ValueChangedEventHandler ValueChanged;
        
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }


            }
        }
        public void OnValueChanged(T value)
        {
            if (ValueChanged != null)
            {
                try
                {
                    ValueChanged(value);
                }
                catch
                {
                    throw;
                }
            }
        }
        
        #endregion


    }
}

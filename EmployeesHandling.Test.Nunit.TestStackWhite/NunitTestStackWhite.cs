﻿using EmployeesHandling.BusinessLogic;
using NUnit.Framework;
using System.IO;
using System.Threading;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace EmployeesHandling.Test.Nunit.TestStackWhite
{
    [TestFixture]
    public class NunitTestStackWhite
    {
        #region Constructor
        public NunitTestStackWhite()
        {

        }
        #endregion

        #region Methods test
        [OneTimeSetUp]
        public void OneTimeInit()
        {
            m_myBusinessLogic = new MyBusinessLogic();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            
        }

        [Test]
        public void TestTheBusinessLogic()
        {
            string name = m_myBusinessLogic.ReadMockup();
            Assert.AreEqual("toto", name);
        }

        [Test]
        public void TestSimulateDragDropUI()
        {
            var applicationPath = Path.Combine(m_strPath, m_strExeName);
            Application application = Application.Launch(applicationPath);
            Window window = application.GetWindow("My Versioned WPF App", InitializeOption.NoCache);

            var _stackWhiteUIElement = window.Get<Button>("ButtonDrop");
            _stackWhiteUIElement.Click();
            
            /*The method for simulating drag and drop is less elegant than in FLAUI*/
            var ptCenter = _stackWhiteUIElement.ClickablePoint;
            System.Windows.Point ptTxtFile = new System.Windows.Point(1100, 140);
            //hardcoded. Have to find a more elegant way
            
            TestStack.White.InputDevices.Mouse.Instance.Location = ptTxtFile;
            Mouse.LeftDown();
            Thread.Sleep(500);
                        
            TestStack.White.InputDevices.Mouse.Instance.Location = ptCenter;
            Mouse.LeftUp();
            /*End of drag and drop*/

            /*This method needs a UI Element, cannot be done from outside the app.*/
            //Mouse.Instance.DragAndDrop(null, ptTxtFile, null, ptCenter);

        }


        [Test]
        [Repeat(1)]
        public void TestClick()
        {
            var applicationPath = Path.Combine(m_strPath, m_strExeName);
            Application application = Application.Launch(applicationPath);
            Window window = application.GetWindow("My Versioned WPF App", InitializeOption.NoCache);

            Button button = window.Get<Button>("ButtonRead");
            button.Click();
        }
        #endregion
        #region Members Private
        string m_strPath = @"C:\Users\SPI\source\repos\EmployeesHandling\EmployeesHandling.UI.WPF\bin\Debug\";
        string m_strExeName = "EmployeesHandling.exe";
        MyBusinessLogic m_myBusinessLogic;
        #endregion

    }
}

﻿using EmployeesHandling.BusinessLogic;
using EmployeesHandling.DatabaseLayer;
using EmployeesHandling.UI.ASPMVC.Business;
using System.Collections.Generic;

namespace EmployeesHandling.UI.ASPMVC.Models
{
    public class MyModel
    {
        public MyModel()
        {
            m_businessLocal = new BusinessLocalInMVC();
            CalcRandom(100);
            m_myBusinessLogic = new MyBusinessLogic();
            ListEmployees = m_myBusinessLogic.Read();
        }

        public void Read()
        {
            ListEmployees = m_myBusinessLogic.Read();
            
        }

        public void Insert(Employee employee)
        {
            m_myBusinessLogic.Insert(employee);
        }
        public void Update(Employee employee)
        {
            m_myBusinessLogic.Update(employee);
        }

        public void CalcRandom(int range)
        {
            TodayCode = m_businessLocal.CalcRandom(range);
        }
        


        private BusinessLocalInMVC m_businessLocal;
        public int TodayCode { get; set; }
        private MyBusinessLogic m_myBusinessLogic;
        public IEnumerable<Employee> ListEmployees{ get; set; }
        


    }
}